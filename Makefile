PROJECT=cli
DESTDIR=/usr
CONFDIR=/etc

M = $(shell printf "\033[34;1m▶\033[0m")

all: build

build: ; $(info $(M) Building project...)
	@./tools/build

# FIXME: this assumes master branch and os/arch, consider using build output
install: build ; $(info $(M) Installing plantd CLI...)
	@install -Dm 755 bin/cli-master-linux-amd64 "$(DESTDIR)/bin/apexcli"
	@install -Dm 644 README.md "$(DESTDIR)/share/doc/plantd/cli/README"
	@install -Dm 644 LICENSE "$(DESTDIR)/share/licenses/plantd/cli/COPYING"
	@mkdir -p "$(CONFDIR)/plantd"
	@install -Dm 644 configs/cli.yaml "$(CONFDIR)/plantd/cli.yaml"

uninstall: ; $(info $(M) Uninstalling plantd CLI...)
	@rm "$(DESTDIR)/bin/apexcli"

clean: ; $(info $(M) Removing generated files... )
	@rm -rf bin/

.PHONY: all build install uninstall clean
