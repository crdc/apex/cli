# Apex CLI

[![Build Status](https://gitlab.com/crdc/apex/cli/badges/master/build.svg)](https://gitlab.com/crdc/apex/cli/commits/master)
[![Coverage Report](https://gitlab.com/crdc/apex/cli/badges/master/coverage.svg)](https://gitlab.com/crdc/apex/cli/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/crdc/apex/cli)](https://goreportcard.com/report/gitlab.com/crdc/apex/cli)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

[WIP] CLI utility to interact with Apex services.

## Installation

```sh
make
sudo make install
```

## Generate Completions Files

Command completion files can be generated for `bash` or `zsh`.

```sh
apexcli completion -s bash
apexcli completion -s zsh
```

Load these one time using

```sh
. <(apexcli completion)
```

or every time using

```sh
echo ". <(apexcli completion)" | tee -a ~/.bashrc
```
