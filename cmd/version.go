package cmd

import (
	"fmt"

	"gitlab.com/crdc/apex/cli/pkg"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of apexcli",
	Long:  `Apex client version information.`,
	Run: func(cmd *cobra.Command, args []string) {
		//fmt.Println("Apex CLI utility v0.1 -- HEAD")
		fmt.Println(version.VERSION)
	},
}
