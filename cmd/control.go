package cmd

import (
	"github.com/spf13/cobra"
)

var controlCmd = &cobra.Command{
	Use:   "control",
	Short: "Interface with the control service",
	Run:   control,
}

func init() {
	rootCmd.AddCommand(controlCmd)
}

func control(cmd *cobra.Command, args []string) {
	args = append([]string{"unit", "--name", "control"}, args...)
	rootCmd.SetArgs(args)
	rootCmd.Execute()
}
