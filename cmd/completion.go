package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var (
	shell string

	completionCmd = &cobra.Command{
		Use:   "completion",
		Short: "Generates command completions for bash and zsh",
		Long: `To load completion run

. <(apexcli completion)

To configure your bash shell to load completions for each session add to your bashrc

# ~/.bashrc or ~/.profile
. <(apexcli completion)
`,
		Run: completion,
	}
)

func init() {
	rootCmd.AddCommand(completionCmd)
	completionCmd.Flags().StringVarP(&shell, "shell", "s", "bash", "shell to generate for [bash|zsh]")
}

func completion(cmd *cobra.Command, args []string) {
	switch shell {
	case "bash":
		rootCmd.GenBashCompletion(os.Stdout)
		break
	case "zsh":
		rootCmd.GenZshCompletion(os.Stdout)
		break
	default:
		fmt.Errorf("Invalid shell choice was provided")
	}
}
