package cmd

import (
	"fmt"

	"gitlab.com/crdc/apex/cli/pkg/client"

	"github.com/spf13/cobra"
)

var (
	configureUpdateCmd = &cobra.Command{
		Use:   "update",
		Short: "Update a configuration by ID",
		Run: func(cmd *cobra.Command, args []string) {
			c, err := client.NewConfigureClient()
			if err != nil {
				fmt.Errorf("error creating client: %s", err)
			}

			if err := c.Update(id, namespace); err != nil {
				fmt.Errorf("failed to update configuration: %s", err)
			}
		},
	}
)

func init() {
	configureCmd.AddCommand(configureUpdateCmd)

	configureUpdateCmd.Flags().StringVar(&id, "id", "", "ID of the configuration to update")
	configureUpdateCmd.Flags().StringVar(&namespace, "namespace", "", "Namespace of the configuration to update")
}
