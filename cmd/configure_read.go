package cmd

import (
	"fmt"

	"gitlab.com/crdc/apex/cli/pkg/client"

	"github.com/spf13/cobra"
)

var (
	configureReadCmd = &cobra.Command{
		Use:   "read",
		Short: "Read a configuration by ID",
		Run: func(cmd *cobra.Command, args []string) {
			c, err := client.NewConfigureClient()
			if err != nil {
				fmt.Errorf("error creating client: %s", err)
			}

			if err := c.Read(id, namespace); err != nil {
				fmt.Errorf("failed to read configuration: %s", err)
			}
		},
	}
)

func init() {
	configureCmd.AddCommand(configureReadCmd)

	configureReadCmd.Flags().StringVar(&id, "id", "", "ID of the configuration to lookup")
	configureReadCmd.Flags().StringVar(&namespace, "namespace", "", "Namespace of the configuration to lookup")
}
