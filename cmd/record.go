package cmd

import (
	"github.com/spf13/cobra"
)

var recordCmd = &cobra.Command{
	Use:   "record",
	Short: "Interface with the record service",
	Run:   record,
}

func init() {
	rootCmd.AddCommand(recordCmd)
}

func record(cmd *cobra.Command, args []string) {
	args = append([]string{"unit", "--name", "record"}, args...)
	rootCmd.SetArgs(args)
	rootCmd.Execute()
}
