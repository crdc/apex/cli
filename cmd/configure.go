package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	id        string
	namespace string

	configureCmd = &cobra.Command{
		Use:   "configure",
		Short: "Communicate with the configuration service",
	}
)

func init() {
	rootCmd.AddCommand(configureCmd)

	configureCmd.Flags().StringP("host", "", "localhost", "the gRPC host to use")
	configureCmd.Flags().IntP("port", "", 4000, "the gRPC port to use")

	viper.BindPFlag("configure.host", configureCmd.Flags().Lookup("host"))
	viper.BindPFlag("configure.port", configureCmd.Flags().Lookup("port"))
}
