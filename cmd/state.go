package cmd

import (
	"github.com/spf13/cobra"
)

var stateCmd = &cobra.Command{
	Use:   "state",
	Short: "Interface with the state service",
	Run:   state,
}

func init() {
	rootCmd.AddCommand(stateCmd)
}

func state(cmd *cobra.Command, args []string) {
	args = append([]string{"unit", "--name", "state"}, args...)
	rootCmd.SetArgs(args)
	rootCmd.Execute()
}
