package cmd

import (
	"github.com/spf13/cobra"
)

var analyzeCmd = &cobra.Command{
	Use:   "analyze",
	Short: "Interface with the analyze service",
	Run:   analyze,
}

func init() {
	rootCmd.AddCommand(analyzeCmd)
}

func analyze(cmd *cobra.Command, args []string) {
	args = append([]string{"unit", "--name", "analyze"}, args...)
	rootCmd.SetArgs(args)
	rootCmd.Execute()
}
