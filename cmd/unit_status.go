package cmd

import (
	"log"

	"gitlab.com/crdc/apex/cli/pkg/client"

	"github.com/spf13/cobra"
)

var unitStatusCmd = &cobra.Command{
	Use:   "status",
	Short: "Retrieve the unit's status",
	Run:   unitStatus,
}

func init() {
	unitCmd.AddCommand(unitStatusCmd)
}

func unitStatus(cmd *cobra.Command, args []string) {
	c, err := client.NewClient(unit, Protocol)
	if err != nil {
		log.Fatalf("Error creating client: %s", err)
	}

	if err := c.GetStatus(); err != nil {
		log.Fatalf("Failed to read status: %s", err)
	}
}
