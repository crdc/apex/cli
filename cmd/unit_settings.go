package cmd

import (
	"log"

	"gitlab.com/crdc/apex/cli/pkg/client"

	"github.com/spf13/cobra"
)

var unitSettingsCmd = &cobra.Command{
	Use:   "settings",
	Short: "Retrieve the unit's settings",
	Run:   unitSettings,
}

func init() {
	unitCmd.AddCommand(unitSettingsCmd)
}

func unitSettings(cmd *cobra.Command, args []string) {
	c, err := client.NewClient(unit, Protocol)
	if err != nil {
		log.Fatalf("Error creating client: %s", err)
	}

	if err := c.GetSettings(); err != nil {
		log.Fatalf("Failed to read settings: %s", err)
	}
}
