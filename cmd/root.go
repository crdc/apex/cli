package cmd

import (
	"fmt"
	"log"
	"os"

	//"gitlab.com/crdc/apex/cli/cmd/acquire"

	homedir "github.com/mitchellh/go-homedir"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	// Blank import here is what's recommended by viper
	_ "github.com/spf13/viper/remote"
)

var (
	cfgFile   string
	remoteCfg string
	// Protocol is used by subcommands to handle different options
	Protocol string
	// Verbose determines whether extra output should be provided
	Verbose bool

	rootCmd = &cobra.Command{
		Use:   "apexcli",
		Short: "Client app to interface with Apex services",
		Long:  `A gRPC, GraphQL, and ZeroMQ client that communicates with various Apex services.`,
	}
)

// Execute function here is used by cobra
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.config/apex/cli/config)")
	rootCmd.PersistentFlags().StringVar(&remoteCfg, "remote-config", "", "use a remote key value store for configuration")
	rootCmd.PersistentFlags().StringVarP(&Protocol, "proto", "p", "grpc", "specify a protocol [grpc|gql|zmq]")
	rootCmd.PersistentFlags().BoolVarP(&Verbose, "verbose", "v", false, "verbose output")

	viper.BindPFlag("verbose", rootCmd.PersistentFlags().Lookup("verbose"))
	viper.SetDefault("verbose", false)

	viper.BindPFlag("proto", rootCmd.PersistentFlags().Lookup("proto"))
	viper.SetDefault("proto", "grpc")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if remoteCfg != "" {
		fmt.Println("Try loading config from consul from:", remoteCfg)
		viper.AddRemoteProvider("consul", remoteCfg, "/plantd/cli/config.json")
		viper.SetConfigType("json")
		err := viper.ReadRemoteConfig()
		if err != nil {
			fmt.Errorf("fatal error reading remote config data: %s", err)
		}
	} else {
		if cfgFile != "" {
			// Use config file from the flag.
			viper.SetConfigFile(cfgFile)
		} else {
			// Find home directory.
			home, err := homedir.Dir()
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}

			// Search config in home directory with name "config.yaml".
			viper.SetConfigName("cli")
			viper.AddConfigPath("/etc/plantd")
			viper.AddConfigPath(".")
			viper.AddConfigPath(fmt.Sprintf("%s/.config/plantd", home))
		}

		viper.AutomaticEnv()

		err := viper.ReadInConfig()
		if err != nil {
			fmt.Errorf("fatal error reading config file: %s", err)
		}
	}
}
