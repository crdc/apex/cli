package cmd

import (
	"log"

	"gitlab.com/crdc/apex/cli/pkg/client"

	"github.com/spf13/cobra"
)

var unitConfigurationCmd = &cobra.Command{
	Use:   "configuration",
	Short: "Retrieve the unit's configuration",
	Run:   unitConfiguration,
}

func init() {
	unitCmd.AddCommand(unitConfigurationCmd)
}

func unitConfiguration(cmd *cobra.Command, args []string) {
	c, err := client.NewClient(unit, Protocol)
	if err != nil {
		log.Fatalf("Error creating client: %s", err)
	}

	// FIXME: this expects an ID, it shouldn't
	if err := c.GetConfiguration(); err != nil {
		log.Fatalf("Failed to read configuration: %s", err)
	}
}
