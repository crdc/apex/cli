package cmd

import (
	"fmt"

	"gitlab.com/crdc/apex/cli/cmd/module"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	unit string

	unitCmd = &cobra.Command{
		Use:   "unit",
		Short: "Interface with the unit service",
	}
)

var unitModuleConfigurationCmd = &cobra.Command{}
var unitModuleEventCmd = &cobra.Command{}
var unitModuleJobCmd = &cobra.Command{}
var unitModulePropertyCmd = &cobra.Command{}
var unitModulePropertiesCmd = &cobra.Command{}
var unitModuleSettingsCmd = &cobra.Command{}
var unitModuleStatusCmd = &cobra.Command{}
var unitModuleShutdownCmd = &cobra.Command{}

func init() {
	rootCmd.AddCommand(unitCmd)

	// Add module commnads
	*unitModuleConfigurationCmd = *module.ConfigurationCmd
	*unitModuleEventCmd = *module.EventCmd
	*unitModuleJobCmd = *module.JobCmd
	*unitModulePropertyCmd = *module.PropertyCmd
	*unitModulePropertiesCmd = *module.PropertiesCmd
	*unitModuleSettingsCmd = *module.SettingsCmd
	*unitModuleStatusCmd = *module.StatusCmd
	*unitModuleShutdownCmd = *module.ShutdownCmd

	unitCmd.AddCommand(unitModuleConfigurationCmd)
	unitCmd.AddCommand(unitModuleEventCmd)
	unitCmd.AddCommand(unitModuleJobCmd)
	unitCmd.AddCommand(unitModulePropertyCmd)
	unitCmd.AddCommand(unitModulePropertiesCmd)
	unitCmd.AddCommand(unitModuleSettingsCmd)
	unitCmd.AddCommand(unitModuleStatusCmd)
	unitCmd.AddCommand(unitModuleShutdownCmd)

	// TODO: make mandatory
	unitCmd.PersistentFlags().StringVarP(&unit, "name", "n", "", "the unit service name to launch")
	viper.BindPFlag("unit", unitCmd.PersistentFlags().Lookup("name"))

	unitCmd.Flags().IntP("rpc-port", "", 4042, "the gRPC port to use")
	unitCmd.Flags().StringP("rpc-host", "", "localhost", "the gRPC host to use")
	unitCmd.Flags().StringP("mq-endpoint", "", "ipc://localhost:7200", "the message queue endpoint to use")

	confRPCPort := fmt.Sprintf("broker.rpc.%s.port", unit)
	confRPCHost := fmt.Sprintf("broker.rpc.%s.host", unit)
	confMQEndpoint := fmt.Sprintf("broker.mq.endpoint", unit)
	viper.BindPFlag(confRPCPort, unitCmd.Flags().Lookup("rpc-port"))
	viper.BindPFlag(confRPCHost, unitCmd.Flags().Lookup("rpc-host"))
	viper.BindPFlag(confMQEndpoint, unitCmd.Flags().Lookup("mq-endpoint"))
}
