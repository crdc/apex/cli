package cmd

import (
	"fmt"

	"gitlab.com/crdc/apex/cli/pkg/client"

	"github.com/spf13/cobra"
)

var (
	configureCreateCmd = &cobra.Command{
		Use:   "create",
		Short: "Create a new configuration",
		Run: func(cmd *cobra.Command, args []string) {
			c, err := client.NewConfigureClient()
			if err != nil {
				fmt.Errorf("error creating client: %s", err)
			}

			if err := c.Create(namespace); err != nil {
				fmt.Errorf("failed to create configuration: %s", err)
			}
		},
	}
)

func init() {
	configureCmd.AddCommand(configureCreateCmd)

	configureCreateCmd.Flags().StringVar(&namespace, "namespace", "", "Namespace of the configuration to lookup")
}
