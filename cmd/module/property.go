package module

import (
	"fmt"

	"gitlab.com/crdc/apex/cli/pkg/client"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	propKey   string
	propValue string

	// PropertyCmd is used by all service subcommands
	PropertyCmd = &cobra.Command{
		Use:   "module-property",
		Short: "Get a module property, provide value to set",
		Run:   moduleProperty,
	}
)

func init() {
	PropertyCmd.PersistentFlags().StringVarP(&moduleName, "module", "m", "", "module name to call")
	PropertyCmd.MarkFlagRequired("module")
	viper.BindPFlag("module", PropertyCmd.PersistentFlags().Lookup("module"))

	PropertyCmd.PersistentFlags().StringVarP(&propKey, "key", "", "", "property key to query/set")
	PropertyCmd.PersistentFlags().StringVarP(&propValue, "value", "", "", "property value to set")
}

func moduleProperty(cmd *cobra.Command, args []string) {
	proto := viper.GetString("proto")
	service := viper.GetString("unit")
	c, err := client.NewClient(service, proto)
	if err != nil {
		fmt.Printf("Error creating client: %s", err)
	}

	if propValue == "" {
		if err := c.GetModuleProperty(moduleName, propKey); err != nil {
			fmt.Printf("Failed to get %s property %s: %s", moduleName, propKey, err)
		}
	} else {
		if err := c.SetModuleProperty(moduleName, propKey, propValue); err != nil {
			fmt.Printf("Failed to set %s property %s: %s", moduleName, propKey, err)
		}
	}
}
