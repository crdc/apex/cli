package module

import (
	"fmt"

	"gitlab.com/crdc/apex/cli/pkg/client"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	propKeys   string
	propValues string

	// PropertiesCmd is used by all service subcommands
	PropertiesCmd = &cobra.Command{
		Use:   "module-properties",
		Short: "Get a module properties, provide value to set",
		Run:   moduleProperties,
	}
)

func init() {
	PropertiesCmd.PersistentFlags().StringVarP(&moduleName, "module", "m", "", "module name to call")
	PropertiesCmd.MarkFlagRequired("module")
	viper.BindPFlag("module", PropertiesCmd.PersistentFlags().Lookup("module"))

	PropertiesCmd.PersistentFlags().StringVarP(&propKeys, "keys", "", "", "properties keys to query/set")
	PropertiesCmd.PersistentFlags().StringVarP(&propValues, "values", "", "", "properties values to set")
}

func moduleProperties(cmd *cobra.Command, args []string) {
	proto := viper.GetString("proto")
	service := viper.GetString("unit")
	c, err := client.NewClient(service, proto)
	if err != nil {
		fmt.Printf("Error creating client: %s", err)
	}

	if propValues == "" {
		if err := c.GetModuleProperties(moduleName, propKeys); err != nil {
			fmt.Printf("Failed to get %s properties %s: %s", moduleName, propKeys, err)
		}
	} else {
		if err := c.SetModuleProperties(moduleName, propKeys, propValues); err != nil {
			fmt.Printf("Failed to set %s properties %s: %s", moduleName, propKeys, err)
		}
	}
}
