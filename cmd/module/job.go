package module

import (
	"fmt"

	"gitlab.com/crdc/apex/cli/pkg/client"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	jobProc string
	jobID   string
	jobName string

	// JobCmd is used by all service subcommands
	JobCmd = &cobra.Command{
		Use:   "module-job",
		Short: "Retrieve the module job",
		Run:   moduleJob,
	}
)

func init() {
	JobCmd.PersistentFlags().StringVarP(&moduleName, "module", "m", "", "module name to call")
	JobCmd.MarkFlagRequired("module")
	viper.BindPFlag("module", JobCmd.PersistentFlags().Lookup("module"))

	JobCmd.PersistentFlags().StringVarP(&jobID, "id", "", "", "job ID to query")
	JobCmd.PersistentFlags().StringVarP(&jobName, "job-name", "j", "", "job name to submit")
	JobCmd.PersistentFlags().StringVarP(&jobProc,
		"exec", "x", "", "job procedure to execute [get|get-all|get-active|submit|cancel]")
}

func moduleJob(cmd *cobra.Command, args []string) {
	proto := viper.GetString("proto")
	service := viper.GetString("unit")
	c, err := client.NewClient(service, proto)
	if err != nil {
		fmt.Printf("Error creating client: %s", err)
	}

	switch jobProc {
	case "get":
		if err := c.GetModuleJob(moduleName, jobID); err != nil {
			fmt.Printf("Failed to read %s job: %s", moduleName, err)
		}
		break
	case "get-all":
		if err := c.GetModuleJobs(moduleName); err != nil {
			fmt.Printf("Failed to read %s jobs: %s", moduleName, err)
		}
		break
	case "get-active":
		if err := c.GetModuleActiveJob(moduleName); err != nil {
			fmt.Printf("Failed to read %s active job: %s", moduleName, err)
		}
		break
	case "submit":
		if err := c.ModuleSubmitJob(moduleName, jobName); err != nil {
			fmt.Printf("Failed to submit %s job: %s", moduleName, err)
		}
		break
	case "cancel":
		if err := c.ModuleCancelJob(moduleName, jobID); err != nil {
			fmt.Printf("Failed to read %s job: %s", moduleName, err)
		}
		break
	default:
		cmd.Usage()
	}

	// TODO: check client error for wrong proto
	switch proto {
	case "grpc":
	case "zmq":
		break
	default:
		cmd.Usage()
	}
}
