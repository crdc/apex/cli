package module

import (
	"fmt"

	"gitlab.com/crdc/apex/cli/pkg/client"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	// ShutdownCmd is used by all service subcommands
	ShutdownCmd = &cobra.Command{
		Use:   "module-shutdown",
		Short: "Send a module shutdown request",
		Run:   moduleShutdown,
	}
)

func init() {
	ShutdownCmd.PersistentFlags().StringVarP(&moduleName, "module", "m", "", "module name to call")
	ShutdownCmd.MarkFlagRequired("module")
	viper.BindPFlag("module", ShutdownCmd.PersistentFlags().Lookup("module"))
}

func moduleShutdown(cmd *cobra.Command, args []string) {
	proto := viper.GetString("proto")
	service := viper.GetString("unit")
	c, err := client.NewClient(service, proto)
	if err != nil {
		fmt.Printf("Error creating client: %s", err)
	}

	if err := c.ModuleShutdown(moduleName); err != nil {
		fmt.Printf("Failed to read status: %s", err)
	}

	switch proto {
	case "grpc":
	case "zmq":
		break
	default:
		cmd.Usage()
	}
}
