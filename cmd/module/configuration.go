package module

import (
	"fmt"

	"gitlab.com/crdc/apex/cli/pkg/client"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	// ConfigurationCmd is used by all service subcommands
	ConfigurationCmd = &cobra.Command{
		Use:   "module-configuration",
		Short: "Retrieve the module configuration",
		Run:   moduleConfiguration,
	}
)

func init() {
	ConfigurationCmd.PersistentFlags().StringVarP(&moduleName, "module", "m", "", "module name to call")
	ConfigurationCmd.MarkFlagRequired("module")
	viper.BindPFlag("module", ConfigurationCmd.PersistentFlags().Lookup("module"))
}

func moduleConfiguration(cmd *cobra.Command, args []string) {
	proto := viper.GetString("proto")
	service := viper.GetString("unit")
	c, err := client.NewClient(service, proto)
	if err != nil {
		fmt.Printf("Error creating client: %s\n", err)
	}

	if err := c.GetModuleConfiguration(moduleName); err != nil {
		fmt.Printf("Failed to read %s configuration: %s\n", moduleName, err)
	}

	switch proto {
	case "grpc":
	case "zmq":
		break
	default:
		cmd.Usage()
	}
}
