package module

import (
	"fmt"

	"gitlab.com/crdc/apex/cli/pkg/client"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	// SettingsCmd is used by all service subcommands
	SettingsCmd = &cobra.Command{
		Use:   "module-settings",
		Short: "Retrieve the module settings",
		Run:   moduleSettings,
	}
)

func init() {
	SettingsCmd.PersistentFlags().StringVarP(&moduleName, "module", "m", "", "module name to call")
	SettingsCmd.MarkFlagRequired("module")
	viper.BindPFlag("module", SettingsCmd.PersistentFlags().Lookup("module"))
}

func moduleSettings(cmd *cobra.Command, args []string) {
	proto := viper.GetString("proto")
	service := viper.GetString("unit")
	c, err := client.NewClient(service, proto)
	if err != nil {
		fmt.Printf("Error creating client: %s", err)
	}

	if err := c.GetModuleSettings(moduleName); err != nil {
		fmt.Printf("Failed to read %s settings: %s", moduleName, err)
	}

	// TODO: check client error for wrong proto
	switch proto {
	case "grpc":
	case "zmq":
		break
	default:
		cmd.Usage()
	}
}
