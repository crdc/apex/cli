package module

import (
	"fmt"

	"gitlab.com/crdc/apex/cli/pkg/client"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	moduleName string

	// StatusCmd is used by all service subcommands
	StatusCmd = &cobra.Command{
		Use:   "module-status",
		Short: "Retrieve the module status",
		Run:   moduleStatus,
	}
)

func init() {
	StatusCmd.PersistentFlags().StringVarP(&moduleName, "module", "m", "", "module name to call")
	StatusCmd.MarkFlagRequired("module")
	viper.BindPFlag("module", StatusCmd.PersistentFlags().Lookup("module"))
}

func moduleStatus(cmd *cobra.Command, args []string) {
	proto := viper.GetString("proto")
	service := viper.GetString("unit")
	c, err := client.NewClient(service, proto)
	if err != nil {
		fmt.Printf("Error creating client: %s", err)
	}

	// FIXME: this expects the ID, not the module name
	if err := c.GetModuleStatus(moduleName); err != nil {
		fmt.Printf("Failed to read status: %s", err)
	}

	switch proto {
	case "grpc":
	case "zmq":
		break
	default:
		cmd.Usage()
	}
}
