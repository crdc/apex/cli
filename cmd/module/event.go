package module

import (
	"fmt"

	"gitlab.com/crdc/apex/cli/pkg/client"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	eventProc string

	// EventCmd is used by all service subcommands
	EventCmd = &cobra.Command{
		Use:   "module-event",
		Short: "Retrieve the module event",
		Run:   moduleEvent,
	}
)

func init() {
	EventCmd.PersistentFlags().StringVarP(&moduleName, "module", "m", "", "module name to call")
	EventCmd.MarkFlagRequired("module")
	viper.BindPFlag("module", EventCmd.PersistentFlags().Lookup("module"))

	EventCmd.PersistentFlags().StringVarP(&eventProc, "exec", "x", "", "event procedure to execute [submit|available]")
}

func moduleEvent(cmd *cobra.Command, args []string) {
	proto := viper.GetString("proto")
	service := viper.GetString("unit")
	c, err := client.NewClient(service, proto)
	if err != nil {
		fmt.Printf("Error creating client: %s", err)
	}

	switch eventProc {
	case "available":
		if err := c.ModuleAvailableEvents(moduleName); err != nil {
			fmt.Printf("Failed to read %s event: %s", moduleName, err)
		}
		break
	case "submit":
		if err := c.ModuleSubmitEvent(moduleName); err != nil {
			fmt.Printf("Failed to read %s event: %s", moduleName, err)
		}
		break
	default:
		cmd.Usage()
	}

	// TODO: check client error for wrong proto
	switch proto {
	case "grpc":
	case "zmq":
		break
	default:
		cmd.Usage()
	}
}
