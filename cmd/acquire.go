package cmd

import (
	"github.com/spf13/cobra"
)

var acquireCmd = &cobra.Command{
	Use:   "acquire",
	Short: "Interface with the acquire service",
	Run:   acquire,
}

func init() {
	rootCmd.AddCommand(acquireCmd)
}

func acquire(cmd *cobra.Command, args []string) {
	args = append([]string{"unit", "--name", "acquire"}, args...)
	rootCmd.SetArgs(args)
	rootCmd.Execute()
}
