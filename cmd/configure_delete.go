package cmd

import (
	"fmt"

	"gitlab.com/crdc/apex/cli/pkg/client"

	"github.com/spf13/cobra"
)

var (
	configureDeleteCmd = &cobra.Command{
		Use:   "delete",
		Short: "Delete a configuration by ID",
		Run: func(cmd *cobra.Command, args []string) {
			c, err := client.NewConfigureClient()
			if err != nil {
				fmt.Errorf("error creating client: %s", err)
			}

			if err := c.Delete(id, namespace); err != nil {
				fmt.Errorf("failed to delete configuration: %s", err)
			}
		},
	}
)

func init() {
	configureCmd.AddCommand(configureDeleteCmd)

	configureDeleteCmd.Flags().StringVar(&id, "id", "", "ID of the configuration to delete")
	configureDeleteCmd.Flags().StringVar(&namespace, "namespace", "", "Namespace of the configuration to delete")
}
