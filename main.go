//go:generate protoc --go_out=plugins=grpc:. proto/apex/core.proto

package main

import (
	"gitlab.com/crdc/apex/cli/cmd"
)

func main() {
	cmd.Execute()
}
