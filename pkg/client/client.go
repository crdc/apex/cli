package client

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"strings"

	"gitlab.com/crdc/apex/go-apex/api"
	pb "gitlab.com/crdc/apex/go-apex/proto/v1"

	"github.com/golang/protobuf/jsonpb"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
)

// TODO: rename PlantClient to differentiate from MasterClient to be added

// Client is used to make service calls over gRPC and ZeroMQ
type Client struct {
	service    string
	protocol   string
	Connection interface{}
}

// NewClient creates a new Client type
func NewClient(service string, protocol string) (client *Client, err error) {
	var c interface{}
	switch protocol {
	case "grpc":
		rpcHost := fmt.Sprintf("broker.rpc.%s.host", service)
		rpcPort := fmt.Sprintf("broker.rpc.%s.port", service)
		rpcAddr := fmt.Sprintf("%s:%d", viper.GetString(rpcHost), viper.GetInt(rpcPort))
		//fmt.Printf("Connect to %s gRPC service at %s\n\n", service, rpcAddr)

		var conn *grpc.ClientConn
		conn, err = grpc.Dial(rpcAddr, grpc.WithInsecure())
		if err != nil {
			return nil, err
		}
		// TODO: figure out how/why this is used
		//defer conn.Close()

		c = pb.NewEndpointClient(conn)
		break
	case "zmq":
		//fmt.Printf("Connect to %s ZeroMQ service at %s\n\n", service, viper.GetString("broker.mq.endpoint"))
		c, err = api.NewClient(viper.GetString("broker.mq.endpoint"))
		if err != nil {
			return nil, err
		}
		break
	default:
		return nil, errors.New("Invalid protocol value provided")
	}

	client = &Client{
		service:    service,
		protocol:   protocol,
		Connection: c,
	}

	return client, nil
}

func (c *Client) ModuleShutdown(name string) (err error) {
	switch c.protocol {
	case "grpc":
		return errors.New("Currently gRPC is not provided for the shutdown call")
	case "zmq":
		request := make([]string, 3)
		request[0] = "module-shutdown"
		request[1] = name
		request[2] = "now"

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		if _, err = c.Connection.(*api.Client).Recv(); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	return nil
}

// GetConfiguration reads the service configuration and prints the result to
// stdout
func (c *Client) GetConfiguration() (err error) {
	var response *pb.ConfigurationResponse
	in := &pb.ConfigurationRequest{}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).GetConfiguration(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "get-configuration"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.ConfigurationResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// GetUnitConfiguration reads the service configuration and prints the result to
// stdout
func (c *Client) GetUnitConfiguration() (err error) {
	var response *pb.ConfigurationResponse
	in := &pb.Empty{}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).GetUnitConfiguration(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "get-unit-configuration"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.ConfigurationResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// GetStatus reads the service status and prints the result to stdout
func (c *Client) GetStatus() (err error) {
	var response *pb.StatusResponse
	in := &pb.StatusRequest{}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).GetStatus(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "get-status"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.StatusResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	// TODO: implement something like this for response codes
	//if response.Code == pb.Response_Code[500]

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// GetSettings reads the service settings and prints the result to stdout
func (c *Client) GetSettings() (err error) {
	var response *pb.SettingsResponse
	in := &pb.SettingsRequest{}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).GetSettings(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "get-settings"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.SettingsResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// GetChannel reads a channel by ID from the service and prints the result to
// stdout
func (c *Client) GetChannel(id string) (err error) {
	var response *pb.ChannelResponse
	in := &pb.ChannelRequest{}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).GetChannel(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "get-channel"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.ChannelResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// GetChannels reads all channels from the service and prints the result to
// stdout
func (c *Client) GetChannels() (err error) {
	var response *pb.ChannelsResponse
	in := &pb.Empty{}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).GetChannels(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "get-channels"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.ChannelsResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// GetJob reads a job by ID from the service and prints the result to stdout
func (c *Client) GetJob(id string) (err error) {
	var response *pb.JobResponse
	in := &pb.JobRequest{Id: id}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).GetJob(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "get-job"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.JobResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// GetJobs reads all jobs from the service and prints the result to stdout
func (c *Client) GetJobs() (err error) {
	var response *pb.JobsResponse
	in := &pb.Empty{}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).GetJobs(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "get-jobs"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.JobsResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// GetJobStatus reads a job status by ID from the service and prints the result
// to stdout
func (c *Client) GetJobStatus(id string) (err error) {
	var response *pb.JobStatusResponse
	in := &pb.JobRequest{Id: id}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).GetJobStatus(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "get-job-status"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.JobStatusResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// GetModule reads a module by ID from the service and prints the result to
// stdout
func (c *Client) GetModule(id string) (err error) {
	var response *pb.ModuleResponse
	in := &pb.ModuleRequest{Id: id}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).GetModule(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "get-module"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.ModuleResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// GetModules reads all modules from the service and prints the result to
// stdout
func (c *Client) GetModules() (err error) {
	var response *pb.ModulesResponse
	in := &pb.Empty{}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).GetModules(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "get-modules"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.ModulesResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// Module specific calls

// GetModuleConfiguration reads a module configuration by ID from the service
// and prints the result to stdout
func (c *Client) GetModuleConfiguration(id string) (err error) {
	var response *pb.ConfigurationResponse
	in := &pb.ModuleRequest{Id: id}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).GetModuleConfiguration(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "get-module-configuration"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.ConfigurationResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// GetModuleStatus reads the status of a module for a given ID from the service
// and prints the result to stdout
func (c *Client) GetModuleStatus(id string) (err error) {
	var response *pb.StatusResponse
	in := &pb.ModuleRequest{Id: id}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).GetModuleStatus(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "get-module-status"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.StatusResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// GetModuleSettings reads the settings of a module for a given ID from the
// service and prints the result to stdout
func (c *Client) GetModuleSettings(id string) (err error) {
	var response *pb.SettingsResponse
	in := &pb.ModuleRequest{Id: id}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).GetModuleSettings(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "get-module-settings"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.SettingsResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// GetModuleJob a job of a module for a given ID from the service and prints the
// result to stdout
func (c *Client) GetModuleJob(id string, jobID string) (err error) {
	var response *pb.JobResponse
	in := &pb.ModuleJobRequest{Id: id, JobId: jobID}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).GetModuleJob(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "get-module-job"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.JobResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// GetModuleJobs reads all jobs of a module for a given ID from the service and
// prints the result to stdout
func (c *Client) GetModuleJobs(id string) (err error) {
	var response *pb.JobsResponse
	in := &pb.ModuleRequest{}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).GetModuleJobs(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "get-module-jobs"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.JobsResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// GetModuleActiveJob reads the active job of a module for a given ID from the
// service and prints the result to stdout
func (c *Client) GetModuleActiveJob(id string) (err error) {
	var response *pb.JobResponse
	in := &pb.ModuleRequest{}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).GetModuleActiveJob(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "get-module-active-job"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.JobResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// ModuleCancelJob attempts to cancel the job of a module for a given module
// and job ID from the service and prints the result to stdout
func (c *Client) ModuleCancelJob(id string, jobID string) (err error) {
	var response *pb.JobResponse
	in := &pb.ModuleJobRequest{Id: id, JobId: jobID}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).ModuleCancelJob(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "module-cancel-job"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.JobResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// ModuleSubmitJob requests a job submission to a module for a given ID from
// the service and prints the job ID received to stdout
func (c *Client) ModuleSubmitJob(id string, name string) (err error) {
	var response *pb.JobResponse
	in := &pb.ModuleJobRequest{Id: id, JobId: name}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).ModuleSubmitJob(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "module-submit-job"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.JobResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// ModuleSubmitEvent requests an event change by a module for a given ID from
// the service and prints the result to stdout
func (c *Client) ModuleSubmitEvent(id string) (err error) {
	var response *pb.JobResponse
	in := &pb.ModuleEventRequest{Id: id}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).ModuleSubmitEvent(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "module-submit-event"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.JobResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// ModuleAvailableEvents reads all events that are provided by a module for a
// given ID from the service and prints the result to stdout
func (c *Client) ModuleAvailableEvents(id string) (err error) {
	var response *pb.EventsResponse
	in := &pb.ModuleRequest{Id: id}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).ModuleAvailableEvents(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "module-available-events"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.EventsResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// GetModuleProperty
func (c *Client) GetModuleProperty(id string, propKey string) (err error) {
	var response *pb.PropertyResponse
	in := &pb.PropertyRequest{Id: id, Key: propKey}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).GetModuleProperty(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "get-module-property"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.PropertyResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// SetModuleProperty
func (c *Client) SetModuleProperty(id string, propKey string, propValue string) (err error) {
	var response *pb.PropertyResponse
	in := &pb.PropertyRequest{Id: id, Key: propKey, Value: propValue}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).SetModuleProperty(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "set-module-property"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.PropertyResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// GetModuleProperties
func (c *Client) GetModuleProperties(id string, propKeys string) (err error) {
	var response *pb.PropertiesResponse
	var properties []*pb.Property
	keys := strings.Split(propKeys, ",")
	for i := 0; i < len(keys); i++ {
		properties = append(properties, &pb.Property{Key: keys[i]})
	}
	in := &pb.PropertiesRequest{Id: id, Properties: properties}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).GetModuleProperties(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "get-module-properties"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.PropertiesResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}

// SetModuleProperties
func (c *Client) SetModuleProperties(id string, propKeys string, propValues string) (err error) {
	var response *pb.PropertiesResponse
	var properties []*pb.Property
	keys := strings.Split(propKeys, ",")
	values := strings.Split(propValues, ",")
	for i := 0; i < len(keys); i++ {
		properties = append(properties, &pb.Property{Key: keys[i], Value: values[i]})
	}
	// TODO: split and iterate keys and values
	in := &pb.PropertiesRequest{Id: id, Properties: properties}
	switch c.protocol {
	case "grpc":
		response, err = c.Connection.(pb.EndpointClient).SetModuleProperties(context.Background(), in)
		if err != nil {
			return err
		}
		break
	case "zmq":
		request := make([]string, 2)
		request[0] = "set-module-properties"
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err = marshaler.Marshal(&b, in); err != nil {
			return err
		}
		request[1] = b.String()

		if err = c.Connection.(*api.Client).Send(c.service, request...); err != nil {
			return err
		}
		// Wait for a response
		var msg []string
		msg, err = c.Connection.(*api.Client).Recv()
		if err != nil {
			return err
		}

		response = &pb.PropertiesResponse{}
		// Only care about the first frame
		if err = jsonpb.Unmarshal(bytes.NewReader([]byte(msg[0])), response); err != nil {
			return err
		}

		break
	default:
		return errors.New("Invalid protocol value provided")
	}

	marshaler := jsonpb.Marshaler{}
	data, _ := marshaler.MarshalToString(response)
	fmt.Printf("%+v\n", data)

	return nil
}
