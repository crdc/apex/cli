package client

import (
	"context"
	"errors"
	"fmt"
	"strings"

	pb "gitlab.com/crdc/apex/go-apex/proto/v1"

	"github.com/golang/protobuf/jsonpb"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
)

// TODO: rename PlantClient to differentiate from MasterClient to be added

// Client is used to make service calls over gRPC and ZeroMQ
type ConfigureClient struct {
	Connection pb.ConfigureEndpointClient
}

// NewClient creates a new Client type
func NewConfigureClient() (client *ConfigureClient, err error) {
	host := viper.GetString("configure.host")
	port := viper.GetInt("configure.port")
	addr := fmt.Sprintf("%s:%d", host, port)

	var conn *grpc.ClientConn
	conn, err = grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	c := pb.NewConfigureEndpointClient(conn)

	client = &ConfigureClient{
		Connection: c,
	}

	return client, nil
}

func (c *ConfigureClient) List(namespace string) (err error) {
	return nil
}

func (c *ConfigureClient) ListAll() (err error) {
	return nil
}

func (c *ConfigureClient) Create(namespace string) (err error) {
	/*
	 *    in := &pb.ConfigurationRequest{}
	 *    response, err := c.Connection.Read(context.Background(), in)
	 *    if err != nil {
	 *        return
	 *    }
	 *
	 *    // Print the response
	 *    fmt.Printf("Configuration:\n%+v\n", response)
	 */

	return nil
}

func (c *ConfigureClient) Read(id string, namespace string) (err error) {
	value, ok := pb.Configuration_Namespace_value[strings.ToUpper(namespace)]
	if !ok {
		return errors.New("invalid namespace provided")
	}

	in := &pb.ConfigurationRequest{
		Id:        id,
		Namespace: pb.Configuration_Namespace(value),
	}
	response, err := c.Connection.Read(context.Background(), in)
	if err != nil {
		return
	}

	m := &jsonpb.Marshaler{}
	json, err := m.MarshalToString(response)
	if err != nil {
		return
	}

	// Print the response
	fmt.Printf("%s\n", json)

	return nil
}

func (c *ConfigureClient) Update(id string, namespace string) (err error) {
	value, ok := pb.Configuration_Namespace_value[strings.ToUpper(namespace)]
	if !ok {
		return errors.New("invalid namespace provided")
	}

	in := &pb.ConfigurationRequest{
		Id:        id,
		Namespace: pb.Configuration_Namespace(value),
	}
	response, err := c.Connection.Update(context.Background(), in)
	if err != nil {
		return
	}

	// Print the response
	fmt.Printf("Updated configuration: %s\n", response.Configuration.Id)

	return nil
}

func (c *ConfigureClient) Delete(id string, namespace string) (err error) {
	value, ok := pb.Configuration_Namespace_value[strings.ToUpper(namespace)]
	if !ok {
		return errors.New("invalid namespace provided")
	}

	in := &pb.ConfigurationRequest{
		Id:        id,
		Namespace: pb.Configuration_Namespace(value),
	}
	response, err := c.Connection.Delete(context.Background(), in)
	if err != nil {
		return
	}

	// Print the response
	fmt.Printf("Deleted configuration: %s\n", response.Configuration.Id)

	return nil
}
